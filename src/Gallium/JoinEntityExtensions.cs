using System;
using System.Collections.Generic;
using System.Linq;

namespace Gallium
{
    public static class JoinEntityExtensions
    {
        /// <summary>
        /// Merges two sets of entities using the identifier to determine which
        /// entities are the same. The `merge` function takes both of the
        /// entities with the Entity from the `input` first and the one from
        /// `other` second. The returning entity is put into the collection. If
        /// an entity from the input is not found in other, then it is just
        /// passed on.
        /// </summary>
        /// <param name="input">The enumerable of entities to merge to.</param>
        /// <param name="other">The collection of entities to merge from.</param>
        /// <param name="merge">The callback to merge the two.</param>
        /// <returns>An sequence of entities, merged and unmerged.</returns>
        public static IEnumerable<Entity> JoinEntity(
            this IEnumerable<Entity> input,
            ICollection<Entity> other,
            Func<Entity, Entity, Entity> merge)
        {
            return input.Join(other, a => a.Id, a => a.Id, merge);
        }
    }
}
