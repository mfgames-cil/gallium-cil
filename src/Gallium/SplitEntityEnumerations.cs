using System.Collections.Generic;

namespace Gallium;

public record SplitEntityEnumerations(
    IEnumerable<Entity> HasAll,
    IEnumerable<Entity> NotHasAll)
{
    /// <summary>
    /// Gets a sequence of all entities that have all the given components.
    /// </summary>
    public IEnumerable<Entity> HasAll { get; } = HasAll;

    /// <summary>
    /// Gets the sequence of all entities that do not have all the given components.
    /// </summary>
    public IEnumerable<Entity> NotHasAll { get; } = NotHasAll;
}
