using System.Collections.Generic;
using System.Linq;

namespace Gallium
{
    public static class WhereEntityHasExtensions
    {
        public static IEnumerable<Entity> WhereEntityHas<T1>(this IEnumerable<Entity> entities)
        {
            return entities.Where(x => x.Has<T1>());
        }

        public static IEnumerable<Entity> WhereEntityHasAll<T1, T2>(this IEnumerable<Entity> entities)
        {
            return entities.Where(x => x.HasAll<T1, T2>());
        }

        public static IEnumerable<Entity> WhereEntityHasAll<T1, T2, T3>(this IEnumerable<Entity> entities)
        {
            return entities.Where(x => x.HasAll<T1, T2, T3>());
        }

        public static IEnumerable<Entity> WhereEntityHasAll<T1, T2, T3, T4>(this IEnumerable<Entity> entities)
        {
            return entities.Where(x => x.HasAll<T1, T2, T3, T4>());
        }
    }
}
