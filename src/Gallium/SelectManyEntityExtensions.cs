using System;
using System.Collections.Generic;
using System.Linq;

namespace Gallium;

/// <summary>
/// An extension method that handle SelectManyEntity which extracts all the
/// entities that match the given components,
/// passes the results to a select many callback, and then optionally includes the
/// ones that didn't have the components
/// before return.
/// </summary>
public static class SelectManyEntityExtensions
{
    /// <summary>
    /// Pulls out all the entities that match the given components into an enumeration,
    /// passes it into the callback
    /// function, and then optionally merges the entities that did not match before
    /// returning.
    /// </summary>
    /// <param name="entities">The entities to process</param>
    /// <param name="selectMany">
    /// The callback function to manipulate the list of
    /// entities.
    /// </param>
    /// <param name="includeEntitiesWithoutComponents">
    /// If true, the include entities
    /// without components.
    /// </param>
    /// <typeparam name="T1">The type of the first component.</typeparam>
    /// <returns>An enumeration of entities.</returns>
    public static IEnumerable<Entity> SelectManyEntity<T1>(
        this IEnumerable<Entity> entities,
        Func<IEnumerable<Entity>, IEnumerable<Entity>> selectMany,
        bool includeEntitiesWithoutComponents = true)
    {
        SplitEntityEnumerations split = entities.SplitEntity<T1>();
        IEnumerable<Entity> results = selectMany(split.HasAll);

        if (includeEntitiesWithoutComponents)
        {
            results = results.Union(split.NotHasAll);
        }

        return results;
    }

    /// <summary>
    /// Pulls out all the entities that match the given components into an enumeration,
    /// passes it into the callback
    /// function, and then optionally merges the entities that did not match before
    /// returning.
    /// </summary>
    /// <param name="entities">The entities to process</param>
    /// <param name="selectMany">
    /// The callback function to manipulate the list of
    /// entities.
    /// </param>
    /// <param name="includeEntitiesWithoutComponents">
    /// If true, the include entities
    /// without components.
    /// </param>
    /// <typeparam name="T1">The type of the first component.</typeparam>
    /// <typeparam name="T2">The type of the second component.</typeparam>
    /// <returns>An enumeration of entities.</returns>
    public static IEnumerable<Entity> SelectManyEntity<T1, T2>(
        this IEnumerable<Entity> entities,
        Func<IEnumerable<Entity>, IEnumerable<Entity>> selectMany,
        bool includeEntitiesWithoutComponents = true)
    {
        SplitEntityEnumerations split = entities.SplitEntity<T1, T2>();
        IEnumerable<Entity> results = selectMany(split.HasAll);

        if (includeEntitiesWithoutComponents)
        {
            results = results.Union(split.NotHasAll);
        }

        return results;
    }

    /// <summary>
    /// Pulls out all the entities that match the given components into an enumeration,
    /// passes it into the callback
    /// function, and then optionally merges the entities that did not match before
    /// returning.
    /// </summary>
    /// <param name="entities">The entities to process</param>
    /// <param name="selectMany">
    /// The callback function to manipulate the list of
    /// entities.
    /// </param>
    /// <param name="includeEntitiesWithoutComponents">
    /// If true, the include entities
    /// without components.
    /// </param>
    /// <typeparam name="T1">The type of the first component.</typeparam>
    /// <typeparam name="T2">The type of the second component.</typeparam>
    /// <typeparam name="T3">The type of the second component.</typeparam>
    /// <returns>An enumeration of entities.</returns>
    public static IEnumerable<Entity> SelectManyEntity<T1, T2, T3>(
        this IEnumerable<Entity> entities,
        Func<IEnumerable<Entity>, IEnumerable<Entity>> selectMany,
        bool includeEntitiesWithoutComponents = true)
    {
        SplitEntityEnumerations split = entities.SplitEntity<T1, T2, T3>();
        IEnumerable<Entity> results = selectMany(split.HasAll);

        if (includeEntitiesWithoutComponents)
        {
            results = results.Union(split.NotHasAll);
        }

        return results;
    }

    /// <summary>
    /// Pulls out all the entities that match the given components into an enumeration,
    /// passes it into the callback
    /// function, and then optionally merges the entities that did not match before
    /// returning.
    /// </summary>
    /// <param name="entities">The entities to process</param>
    /// <param name="selectMany">
    /// The callback function to manipulate the list of
    /// entities.
    /// </param>
    /// <param name="includeEntitiesWithoutComponents">
    /// If true, the include entities
    /// without components.
    /// </param>
    /// <typeparam name="T1">The type of the first component.</typeparam>
    /// <typeparam name="T2">The type of the second component.</typeparam>
    /// <typeparam name="T3">The type of the second component.</typeparam>
    /// <typeparam name="T4">The type of the second component.</typeparam>
    /// <returns>An enumeration of entities.</returns>
    public static IEnumerable<Entity> SelectManyEntity<T1, T2, T3, T4>(
        this IEnumerable<Entity> entities,
        Func<IEnumerable<Entity>, IEnumerable<Entity>> selectMany,
        bool includeEntitiesWithoutComponents = true)
    {
        SplitEntityEnumerations split = entities.SplitEntity<T1, T2, T3, T4>();
        IEnumerable<Entity> results = selectMany(split.HasAll);

        if (includeEntitiesWithoutComponents)
        {
            results = results.Union(split.NotHasAll);
        }

        return results;
    }
}
