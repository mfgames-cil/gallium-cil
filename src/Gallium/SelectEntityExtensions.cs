using System;
using System.Collections.Generic;

namespace Gallium
{
    public static class SelectEntityExtensions
    {
        /// <summary>
        /// Selects an entity from the given list, filtering on entities with
        /// the given components.
        /// </summary>
        /// <param name="entities">The entities to parse.</param>
        /// <param name="selectWithComponents">
        /// The transformation function for the entity and selected components. If this
        /// returns null, then the entity
        /// will be filtered out.
        /// </param>
        /// <param name="includeEntitiesWithoutComponents">
        /// If true, then entities without all the components are included. Otherwise, they
        /// are excluded.
        /// </param>
        /// <typeparam name="T1">The type of the first component.</typeparam>
        /// <returns>An enumeration of transformed entities.</returns>
        public static IEnumerable<Entity> SelectEntity<T1>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, Entity?> selectWithComponents,
            bool includeEntitiesWithoutComponents = true)
        {
            return entities.SelectEntity(selectWithComponents, includeEntitiesWithoutComponents ? a => a : a => null);
        }

        /// <summary>
        /// Selects an entity from the given list, filtering on entities with
        /// the given components.
        /// </summary>
        /// <param name="entities">The entities to parse.</param>
        /// <param name="selectWithComponents">
        /// The transformation function for the entity and selected components. If this
        /// returns null, then the entity
        /// will be filtered out.
        /// </param>
        /// <param name="includeEntitiesWithoutComponents">
        /// If true, then entities without all the components are included. Otherwise, they
        /// are excluded.
        /// </param>
        /// <typeparam name="T1">The type of the first component.</typeparam>
        /// <typeparam name="T2">The type of the second component.</typeparam>
        /// <returns>An enumeration of transformed entities.</returns>
        public static IEnumerable<Entity> SelectEntity<T1, T2>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, Entity?> selectWithComponents,
            bool includeEntitiesWithoutComponents = true)
        {
            return entities.SelectEntity(selectWithComponents, includeEntitiesWithoutComponents ? a => a : a => null);
        }

        /// <summary>
        /// Selects an entity from the given list, filtering on entities with
        /// the given components.
        /// </summary>
        /// <param name="entities">The entities to parse.</param>
        /// <param name="selectWithComponents">
        /// The transformation function for the entity and selected components. If this
        /// returns null, then the entity
        /// will be filtered out.
        /// </param>
        /// <param name="includeEntitiesWithoutComponents">
        /// If true, then entities without all the components are included. Otherwise, they
        /// are excluded.
        /// </param>
        /// <typeparam name="T1">The type of the first component.</typeparam>
        /// <typeparam name="T2">The type of the second component.</typeparam>
        /// <typeparam name="T3">The type of the third component.</typeparam>
        /// <returns>An enumeration of transformed entities.</returns>
        public static IEnumerable<Entity> SelectEntity<T1, T2, T3>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, T3, Entity?> selectWithComponents,
            bool includeEntitiesWithoutComponents = true)
        {
            return entities.SelectEntity(selectWithComponents, includeEntitiesWithoutComponents ? a => a : a => null);
        }

        /// <summary>
        /// Selects an entity from the given list, filtering on entities with
        /// the given components.
        /// </summary>
        /// <param name="entities">The entities to parse.</param>
        /// <param name="selectWithComponents">
        /// The transformation function for the entity and selected components. If this
        /// returns null, then the entity
        /// will be filtered out.
        /// </param>
        /// <param name="includeEntitiesWithoutComponents">
        /// If true, then entities without all the components are included. Otherwise, they
        /// are excluded.
        /// </param>
        /// <typeparam name="T1">The type of the first component.</typeparam>
        /// <typeparam name="T2">The type of the second component.</typeparam>
        /// <typeparam name="T3">The type of the third component.</typeparam>
        /// <typeparam name="T4">The type of the fourth component.</typeparam>
        /// <returns>An enumeration of transformed entities.</returns>
        public static IEnumerable<Entity> SelectEntity<T1, T2, T3, T4>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, T3, T4, Entity?> selectWithComponents,
            bool includeEntitiesWithoutComponents = true)
        {
            return entities.SelectEntity(selectWithComponents, includeEntitiesWithoutComponents ? a => a : a => null);
        }

        /// <summary>
        /// Selects an entity from the given list, filtering on entities with
        /// the given components.
        /// </summary>
        /// <param name="entities">The entities to parse.</param>
        /// <param name="selectWithComponents">
        /// The transformation function for the entity and selected components. If this
        /// returns null, then the entity
        /// will be filtered out.
        /// </param>
        /// <param name="selectWithoutComponents">
        /// The optional transformation function for entities that do not have all the
        /// components. If returns null,
        /// then the entity will not be included.
        /// </param>
        /// <typeparam name="T1">The type of the first component.</typeparam>
        /// <returns>An enumeration of transformed entities.</returns>
        public static IEnumerable<Entity> SelectEntity<T1>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, Entity?> selectWithComponents,
            Func<Entity, Entity?> selectWithoutComponents)
        {
            foreach (Entity entity in entities)
            {
                Entity? result = entity.TryGet(out T1 value1)
                    ? selectWithComponents?.Invoke(entity, value1)
                    : selectWithoutComponents?.Invoke(entity);

                if (result != null)
                {
                    yield return result;
                }
            }
        }

        /// <summary>
        /// Selects an entity from the given list, filtering on entities with
        /// the given components.
        /// </summary>
        /// <param name="entities">The entities to parse.</param>
        /// <param name="selectWithComponents">
        /// The transformation function for the entity and selected components. If this
        /// returns null, then the entity
        /// will be filtered out.
        /// </param>
        /// <param name="selectWithoutComponents">
        /// The optional transformation function for entities that do not have all the
        /// components. If returns null,
        /// then the entity will not be included.
        /// </param>
        /// <typeparam name="T1">The type of the first component.</typeparam>
        /// <typeparam name="T2">The type of the second component.</typeparam>
        /// <returns>An enumeration of transformed entities.</returns>
        public static IEnumerable<Entity> SelectEntity<T1, T2>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, Entity?> selectWithComponents,
            Func<Entity, Entity?> selectWithoutComponents)
        {
            foreach (Entity entity in entities)
            {
                Entity? result = entity.TryGet(out T1 value1) && entity.TryGet(out T2 value2)
                    ? selectWithComponents?.Invoke(entity, value1, value2)
                    : selectWithoutComponents?.Invoke(entity);

                if (result != null)
                {
                    yield return result;
                }
            }
        }

        /// <summary>
        /// Selects an entity from the given list, filtering on entities with
        /// the given components.
        /// </summary>
        /// <param name="entities">The entities to parse.</param>
        /// <param name="selectWithComponents">
        /// The transformation function for the entity and selected components. If this
        /// returns null, then the entity
        /// will be filtered out.
        /// </param>
        /// <param name="selectWithoutComponents">
        /// The optional transformation function for entities that do not have all the
        /// components. If returns null,
        /// then the entity will not be included.
        /// </param>
        /// <typeparam name="T1">The type of the first component.</typeparam>
        /// <typeparam name="T2">The type of the second component.</typeparam>
        /// <typeparam name="T3">The type of the third component.</typeparam>
        /// <returns>An enumeration of transformed entities.</returns>
        public static IEnumerable<Entity> SelectEntity<T1, T2, T3>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, T3, Entity?> selectWithComponents,
            Func<Entity, Entity?> selectWithoutComponents)
        {
            foreach (Entity entity in entities)
            {
                Entity? result =
                    entity.TryGet(out T1 value1) && entity.TryGet(out T2 value2) && entity.TryGet(out T3 value3)
                        ? selectWithComponents?.Invoke(entity, value1, value2, value3)
                        : selectWithoutComponents?.Invoke(entity);

                if (result != null)
                {
                    yield return result;
                }
            }
        }

        /// <summary>
        /// Selects an entity from the given list, filtering on entities with
        /// the given components.
        /// </summary>
        /// <param name="entities">The entities to parse.</param>
        /// <param name="selectWithComponents">
        /// The transformation function for the entity and selected components. If this
        /// returns null, then the entity
        /// will be filtered out.
        /// </param>
        /// <param name="selectWithoutComponents">
        /// The optional transformation function for entities that do not have all the
        /// components. If returns null,
        /// then the entity will not be included.
        /// </param>
        /// <typeparam name="T1">The type of the first component.</typeparam>
        /// <typeparam name="T2">The type of the second component.</typeparam>
        /// <typeparam name="T3">The type of the third component.</typeparam>
        /// <typeparam name="T4">The type of the third component.</typeparam>
        /// <returns>An enumeration of transformed entities.</returns>
        public static IEnumerable<Entity> SelectEntity<T1, T2, T3, T4>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, T3, T4, Entity?> selectWithComponents,
            Func<Entity, Entity?> selectWithoutComponents)
        {
            foreach (Entity entity in entities)
            {
                Entity? result =
                    entity.TryGet(out T1 value1)
                    && entity.TryGet(out T2 value2)
                    && entity.TryGet(out T3 value3)
                    && entity.TryGet(out T4 value4)
                        ? selectWithComponents?.Invoke(entity, value1, value2, value3, value4)
                        : selectWithoutComponents?.Invoke(entity);

                if (result != null)
                {
                    yield return result;
                }
            }
        }
    }
}
