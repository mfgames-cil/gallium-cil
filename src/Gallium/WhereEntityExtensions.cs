using System;
using System.Collections.Generic;
using System.Linq;

namespace Gallium
{
    public static class WhereEntityExtensions
    {
        public static IEnumerable<Entity> WhereEntity<T1>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, bool> include)
        {
            return entities.Where(x => x.Has<T1>() && include(x, x.Get<T1>()));
        }

        public static IEnumerable<Entity> WhereEntity<T1, T2>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, bool> include)
        {
            return entities.Where(
                x => x.HasAll<T1, T2>()
                    && include(x, x.Get<T1>(), x.Get<T2>()));
        }

        public static IEnumerable<Entity> WhereEntity<T1, T2, T3>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, T3, bool> include)
        {
            return entities.Where(
                x => x.HasAll<T1, T2, T3>()
                    && include(x, x.Get<T1>(), x.Get<T2>(), x.Get<T3>()));
        }

        public static IEnumerable<Entity> WhereEntity<T1, T2, T3, T4>(
            this IEnumerable<Entity> entities,
            Func<Entity, T1, T2, T3, T4, bool> include)
        {
            return entities.Where(
                x => x.HasAll<T1, T2, T3, T4>()
                    && include(
                        x,
                        x.Get<T1>(),
                        x.Get<T2>(),
                        x.Get<T3>(),
                        x.Get<T4>()));
        }
    }
}
