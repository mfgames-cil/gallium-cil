# Gallium CIL

A small Entity-Component-System (ECS) that is built around LINQ calls and `IEnumerable<Entity>` objects.
