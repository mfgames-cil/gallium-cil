# Building

## Environment

To simplify environmental setup, this project uses the following:

-   [Nix](https://nixos.org/)
-   [direnv](https://direnv.net/)

Nix does not require the full NixOS (the operating system component), just a single- or multi-user setup. `direnv` is used to automatically configure the environment when entering the directory including setting up the needed PATH and other environment variables for the build.

### Working Without Nix

If not using Nix to set up the environment, look at `buildInputs` in [./shell.nix](./shell.nix) which includes the tools require for building (such as a specific version of Yarn, .NET Core, and the like).

`shell.nix` along with the data inside the `nix/` folder is considered the source of truth of dependencies and requirements for this project.

### Node and Yarn

Node is used as both the package metadata format (package version) and also build processes. We use [Husky]() to automatically set up Git hooks, [commitlint]() to ensure consistent check-ins that work with [semantic-release]() which automates the build process.

Setting up the environment consists of:

```
npm install
```

## Building

The preferred method for building is:

```
npm run build
```

This ensures everything is properly run. A short-cut can be to call the `dotnet build` directly.

```
dotnet build
```

## Testing

Again, the preferred method of testing is:

```
npm test
```
