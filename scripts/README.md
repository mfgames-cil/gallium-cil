# Scripts Directory

This directory contains the basic scripts for working with the library.

## `setup.sh`

This verifies the environment is correct and makes sure everything is configured.

## `build.sh`

This builds the project and creates the binaries in debug mode.

## `test.sh`

This runs any required tests.

## `format.sh`

This is used to format the code base using our standards. It matches the commands in the `lefthook` pre-commit hook.

## `release.sh`

Intended to run in a CI environment, this creates a NuGet package and publishes it.
