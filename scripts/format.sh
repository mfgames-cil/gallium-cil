#!/usr/bin/env sh

# Normalize our environment.
cd $(dirname $0)/..
./scripts/setup.sh || exit 1

# Format the .NET code
dotnet format || exit 1

# Format using Prettier.
prettier . --write --loglevel warn

# Format the Flake.
nixfmt flake.nix
