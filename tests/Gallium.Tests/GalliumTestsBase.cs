using Serilog;
using Serilog.Core;

using Xunit.Abstractions;

namespace Gallium.Tests
{
    /// <summary>
    /// Common initialization logic for Gallium-based tests including setting
    /// up containers, logging, and Serilog.
    /// </summary>
    public abstract class GalliumTestsBase
    {
        protected GalliumTestsBase(ITestOutputHelper output)
        {
            this.Output = output;

            // Set up logging.
            const string Template =
                "[{Level:u3}] "
                + "({SourceContext}) {Message}"
                + "{NewLine}{Exception}";

            this.Logger = new LoggerConfiguration()
                .WriteTo.TestOutput(
                    output,
                    outputTemplate: Template)
                .CreateLogger();
        }

        /// <summary>
        /// Gets the output for the tests.
        /// </summary>
        public ITestOutputHelper Output { get; }

        /// <summary>
        /// Gets the logger used to report messages about the test.
        /// </summary>
        protected Logger Logger { get; }
    }
}
