using System.Linq;

using Xunit;

namespace Gallium.Tests
{
    public class EnumerableEntityTests
    {
        [Fact]
        public void ForComponentsC1()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1()),
                new Entity().Add("2")
                    .Add(new TestComponent2()),
                new Entity().Add("3")
                    .Add(new TestComponent1()),
            };

            Assert.Equal(
                new[] { "1!", "2", "3!" },
                entities.SelectEntity<TestComponent1>(
                        (
                            e,
                            _) => e.Set(e.Get<string>() + "!"))
                    .Select(x => x.Get<string>())
                    .ToArray());
        }

        [Fact]
        public void ForComponentsC2()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1()),
                new Entity().Add("2")
                    .Add(new TestComponent2()),
                new Entity().Add("3")
                    .Add(new TestComponent1())
                    .Add(new TestComponent2()),
            };

            Assert.Equal(
                new[] { "1", "2", "3!" },
                entities.SelectEntity<TestComponent1, TestComponent2>(
                        (
                            e,
                            _,
                            _) => e.Set(e.Get<string>() + "!"))
                    .Select(x => x.Get<string>())
                    .ToArray());
        }

        [Fact]
        public void ForComponentsC3()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1()),
                new Entity().Add("2")
                    .Add(new TestComponent2())
                    .Add<ITestComponent3>(new TestComponent3b()),
                new Entity().Add("3")
                    .Add<ITestComponent3>(new TestComponent3a())
                    .Add(new TestComponent1())
                    .Add(new TestComponent2()),
            };

            Assert.Equal(
                new[] { "1", "2", "3-TestComponent3a" },
                entities.SelectEntity<TestComponent1, TestComponent2, ITestComponent3>(
                        (
                            e,
                            _,
                            _,
                            t) => e.Set(
                            e.Get<string>()
                            + "-"
                            + t.GetType()
                                .Name))
                    .Select(x => x.Get<string>())
                    .ToArray());
        }

        [Fact]
        public void HasComponentsC1()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1()),
                new Entity().Add("2")
                    .Add(new TestComponent2()),
                new Entity().Add("3")
                    .Add(new TestComponent1()),
            };

            Assert.Equal(
                new[] { "1", "3" },
                entities.WhereEntityHas<TestComponent1>()
                    .Select(x => x.Get<string>())
                    .ToArray());
        }

        [Fact]
        public void HasComponentsC2()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1()),
                new Entity().Add("2")
                    .Add(new TestComponent2())
                    .Add(new TestComponent1()),
                new Entity().Add("3")
                    .Add(new TestComponent1()),
            };

            Assert.Equal(
                new[] { "2" },
                entities.WhereEntityHasAll<TestComponent1, TestComponent2>()
                    .Select(x => x.Get<string>())
                    .ToArray());
        }

        [Fact]
        public void HasComponentsC3()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1())
                    .Add<ITestComponent3>(new TestComponent3b())
                    .Add(new TestComponent2()),
                new Entity().Add("2")
                    .Add(new TestComponent2())
                    .Add(new TestComponent1()),
                new Entity().Add("3")
                    .Add<ITestComponent3>(new TestComponent3a()),
            };

            Assert.Equal(
                new[] { "1" },
                entities.WhereEntityHasAll<TestComponent1, TestComponent2, ITestComponent3>()
                    .Select(x => x.Get<string>())
                    .ToArray());
        }

        [Fact]
        public void NotComponentsC1()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1()),
                new Entity().Add("2")
                    .Add(new TestComponent2()),
                new Entity().Add("3")
                    .Add(new TestComponent1()),
            };

            Assert.Equal(
                new[] { "2" },
                entities.WhereEntityNotHas<TestComponent1>()
                    .Select(x => x.Get<string>())
                    .ToArray());
        }

        [Fact]
        public void NotComponentsC2()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1()),
                new Entity().Add("2")
                    .Add(new TestComponent2())
                    .Add(new TestComponent1()),
                new Entity().Add("3"),
            };

            Assert.Equal(
                new[] { "1", "3" },
                entities.WhereEntityNotHasAll<TestComponent1, TestComponent2>()
                    .Select(x => x.Get<string>())
                    .ToArray());
        }

        [Fact]
        public void NotComponentsC3()
        {
            Entity[] entities =
            {
                new Entity().Add("1")
                    .Add(new TestComponent1()),
                new Entity().Add("2")
                    .Add(new TestComponent1())
                    .Add(new TestComponent2())
                    .Add<ITestComponent3>(new TestComponent3b()),
                new Entity().Add("3")
                    .Add<ITestComponent3>(new TestComponent3a()),
            };

            Assert.Equal(
                new string[] { "1", "3" },
                entities.WhereEntityNotHasAll<TestComponent1, TestComponent2, ITestComponent3>()
                    .Select(x => x.Get<string>())
                    .ToArray());
        }
    }
}
