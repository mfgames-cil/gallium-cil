using System;

using Xunit;
using Xunit.Abstractions;

namespace Gallium.Tests
{
    public class EntityTests : GalliumTestsBase
    {
        public EntityTests(ITestOutputHelper output)
            : base(output)
        {
        }

        [Fact]
        public void AddingComponentOnceWorks()
        {
            var component1 = new TestComponent1();
            Entity entity1 = new Entity().Add(component1);

            Assert.Equal(1, entity1.Count);
            Assert.True(entity1.Has<TestComponent1>());
            Assert.Equal(component1, entity1.Get<TestComponent1>());
        }

        [Fact]
        public void AddingComponentViaInterfaceWork()
        {
            var component1 = new TestComponent3a();
            Entity entity1 = new Entity().Add<ITestComponent3>(component1);

            Assert.Equal(1, entity1.Count);
            Assert.False(entity1.Has<TestComponent3a>());
            Assert.True(entity1.Has<ITestComponent3>());
            Assert.Equal(component1, entity1.Get<ITestComponent3>());
        }

        [Fact]
        public void AddingTwiceThrowsException()
        {
            var component1 = new TestComponent1();

            Exception exception = Assert.Throws<ArgumentException>(
                () => new Entity()
                    .Add(component1)
                    .Add(component1));

            Assert.Equal(
                "An element with the same type "
                + "(Gallium.Tests.TestComponent1)"
                + " already exists. (Parameter 'component')",
                exception.Message);
        }

        [Fact]
        public void CopyEntityAlsoCopiesComponents()
        {
            var component1 = new TestComponent1();
            Entity entity1 = new Entity().Add(component1);
            Entity entity2 = entity1.Copy();

            Assert.Equal(1, entity2.Count);
            Assert.True(entity2.Has<TestComponent1>());
            Assert.Equal(component1, entity2.Get<TestComponent1>());
        }

        [Fact]
        public void ExactCopyEntityAlsoCopiesComponents()
        {
            var component1 = new TestComponent1();
            Entity entity1 = new Entity().Add(component1);
            Entity entity2 = entity1.ExactCopy();

            Assert.Equal(1, entity2.Count);
            Assert.True(entity2.Has<TestComponent1>());
            Assert.Equal(component1, entity2.Get<TestComponent1>());
        }

        [Fact]
        public void GetOptionalWorks()
        {
            var component1 = new TestComponent1();
            Entity entity1 = new Entity().Add(component1);

            Assert.Equal(component1, entity1.GetOptional<TestComponent1>());
            Assert.Null(entity1.GetOptional<TestComponent2>());
        }

        [Fact]
        public void HasReturnsFalseIfNotRegistered()
        {
            var entity1 = new Entity();

            Assert.False(entity1.Has<TestComponent1>());
        }

        [Fact]
        public void NewEntityHasNoComponents()
        {
            var entity1 = new Entity();

            Assert.Equal(0, entity1.Count);
        }

        [Fact]
        public void RemoveAlreadyMissingComponentWorks()
        {
            var entity1 = new Entity();

            Assert.Equal(0, entity1.Count);

            Entity entity2 = entity1.Remove<TestComponent1>();

            Assert.Equal(0, entity1.Count);
            Assert.Equal(0, entity2.Count);
        }

        [Fact]
        public void RemoveComponentDoesNotRemoveFromCopies()
        {
            var component1 = new TestComponent1();
            Entity entity1 = new Entity().Add(component1);
            Entity entity2 = entity1.ExactCopy();
            Entity entity3 = entity1.Copy();

            Assert.Equal(1, entity1.Count);
            Assert.Equal(1, entity2.Count);
            Assert.Equal(1, entity3.Count);

            Entity entity1A = entity1.Remove<TestComponent1>();

            Assert.Equal(1, entity1.Count);
            Assert.Equal(0, entity1A.Count);
            Assert.Equal(1, entity2.Count);
            Assert.Equal(1, entity3.Count);
        }

        [Fact]
        public void RemoveComponentWorks()
        {
            var component1 = new TestComponent1();
            Entity entity1 = new Entity().Add(component1);

            Assert.Equal(1, entity1.Count);

            Entity entity2 = entity1.Remove<TestComponent1>();

            Assert.Equal(1, entity1.Count);
            Assert.Equal(0, entity2.Count);
        }

        [Fact]
        public void SettingComponentsWorks()
        {
            var component1 = new TestComponent3a();
            var component2 = new TestComponent3b();

            Entity entity1 = new Entity()
                .Set<ITestComponent3>(component1)
                .Set<ITestComponent3>(component1)
                .Set<ITestComponent3>(component2);

            Assert.Equal(1, entity1.Count);
            Assert.Equal(component2, entity1.Get<ITestComponent3>());
        }

        [Fact]
        public void TryGetWorks()
        {
            var component1 = new TestComponent1();
            Entity entity1 = new Entity().Add(component1);

            bool result1 = entity1.TryGet(out TestComponent1 value1);

            Assert.True(result1);
            Assert.Equal(component1, value1);

            bool result2 = entity1.TryGet(out TestComponent2 _);

            Assert.False(result2);
        }

        [Fact]
        public void TwoCopiesHaveDifferentIds()
        {
            var entity1 = new Entity();
            Entity entity2 = entity1.Copy();

            Assert.NotEqual(entity1.Id, entity2.Id);
            Assert.NotEqual(entity1, entity2);
            Assert.False(entity1 == entity2);
        }

        [Fact]
        public void TwoEntitiesHaveDifferentIds()
        {
            var entity1 = new Entity();
            var entity2 = new Entity();

            Assert.NotEqual(entity1.Id, entity2.Id);
            Assert.NotEqual(entity1, entity2);
            Assert.False(entity1 == entity2);
        }

        [Fact]
        public void TwoExactCopiesHaveDifferentIds()
        {
            var entity1 = new Entity();
            Entity entity2 = entity1.ExactCopy();

            Assert.Equal(entity1.Id, entity2.Id);
            Assert.Equal(entity1, entity2);
            Assert.True(entity1 == entity2);
        }
    }
}
